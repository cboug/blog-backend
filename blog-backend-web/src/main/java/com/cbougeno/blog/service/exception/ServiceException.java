package com.cbougeno.blog.service.exception;

public class ServiceException {

    private static final long serialVersionUID = -8999358340769872358L;

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }
}
