package com.cbougeno.blog.service.bean;

import com.cbougeno.blog.service.model.Article;

import java.util.List;
import java.util.Optional;

public interface ArticleService {
    List<Article> findAll();
    Optional<Article> findById(String id);
}
