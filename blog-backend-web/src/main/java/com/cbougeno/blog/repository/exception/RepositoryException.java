package com.cbougeno.blog.repository.exception;

public class RepositoryException extends RuntimeException {

    private static final long serialVersionUID = -6223336487048941065L;

    public RepositoryException(String message) {
        super(message);
    }

    public RepositoryException(String message, Throwable cause) {
        super(message, cause);
    }

    public RepositoryException(Throwable cause) {
        super(cause);
    }
}
