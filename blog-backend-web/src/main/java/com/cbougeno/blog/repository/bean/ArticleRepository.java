package com.cbougeno.blog.repository.bean;

import com.cbougeno.blog.repository.exception.RepositoryException;
import com.cbougeno.blog.service.model.Article;

import java.util.List;
import java.util.Optional;

public interface ArticleRepository {
    List<Article> findAll() throws RepositoryException;
    Optional<Article> findById(String id) throws RepositoryException;
}
