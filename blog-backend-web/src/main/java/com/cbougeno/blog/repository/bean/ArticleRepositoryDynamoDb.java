package com.cbougeno.blog.repository.bean;

import com.cbougeno.blog.repository.exception.RepositoryException;
import com.cbougeno.blog.service.model.Article;

import java.util.List;
import java.util.Optional;

public class ArticleRepositoryDynamoDb implements ArticleRepository {
    @Override
    public List<Article> findAll() throws RepositoryException {
        return null;
    }

    @Override
    public Optional<Article> findById(String id) throws RepositoryException {
        return Optional.empty();
    }
}
