package com.cbougeno.blog.repository.model;

import java.io.Serializable;

public class ArticleRO implements Serializable {
    private static final long serialVersionUID = -5809496801404727068L;

    private String id;
    private String title;
    private String author;
    private String description;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
